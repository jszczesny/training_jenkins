def info(message) {
    echo "INFO: ${message}"
}

def warning(message) {
    echo "WARNING: ${message}"
}

def erorr(message) {
    echo "ERORR: ${message}"
}
